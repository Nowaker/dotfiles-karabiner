#!/usr/bin/env bash

set -xe

brew install sleepwatcher
sudo cp ~/.config/karabiner/net.nowaker.karabiner-sleepwatcher.plist /Library/LaunchDaemons
sudo launchctl unload /Library/LaunchDaemons/net.nowaker.karabiner-sleepwatcher.plist || true
sudo launchctl load /Library/LaunchDaemons/net.nowaker.karabiner-sleepwatcher.plist

