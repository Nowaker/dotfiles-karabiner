#!/usr/bin/env bash

touch /tmp/works

LOG=/tmp/karabiner-restart.log

if [ "$UID" != 0 ]; then
  MSG="`date` This script needs to run as root."
  echo "$MSG" | tee -a "$LOG"
  exit 1
fi

if [ ! -v PRE ]; then
  PRE=0
fi

if [ ! -v POST ]; then
  POST=0
fi

echo "Pre: $PRE, post: $POST"

sleep "$PRE"
launchctl unload /Library/LaunchDaemons/org.pqrs.karabiner.karabiner_grabber.plist
sleep "$POST"
launchctl load /Library/LaunchDaemons/org.pqrs.karabiner.karabiner_grabber.plist

echo "`date` Karabiner daemon restarted." | tee -a "$LOG"
chmod o+w "$LOG"

