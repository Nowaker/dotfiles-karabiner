# Dotfiles: Karabiner

```
mkdir -p ~/.config/karabiner
cd ~/.config/karabiner
git clone git@gitlab.com:Nowaker/dotfiles-karabiner.git .
```

## MBP with touch bar

There's an issue with remapping Fn physical key -
[tekezo/Karabiner-Elements#1641](https://github.com/tekezo/Karabiner-Elements/issues/1641).
Use SleepWatcher to workaround the problem most of the times:

```
./install-restart-on-wake.sh
```

Sometimes, the SleepWatcher doesn't trigger the script.
In that case you'll need to execute the script yourself:

```
~/.config/karabiner/restart-on-wake.sh
```

